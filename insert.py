from Database import Database
import csv
db = Database()


inserted = 0
count_user = 0
with open('participants-final.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count+=1
            continue
        cursor = db.mydb.cursor()
        sql_select_query = """select * from participants where email = %s"""
        cursor.execute(sql_select_query, (str(row[0]),))
        record = cursor.fetchall()
        if(len(record) == 0):
            query = """INSERT INTO participants (email, name, invitation_status) VALUES(%s, %s, %s)"""
            try : 
                cursor.execute(query, (str(row[0]), str(row[1]), "Belum", ))
                print(f"Inserted : {row[0]} {row[1]} ")
                db.mydb.commit()
                inserted+=1
            except : 
                print("Failed to insert")
        else : 
            print(f"Participant {row[1]} already on db")
print(f"Inserted {inserted} participants to db")

print(f"Total participant after data cleaning : {count_user}")
        

