from Database import Database
from jinja2 import Environment, FileSystemLoader
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP_SSL

db = Database()
cursor = db.mydb.cursor()

sql_select_query = """select * from participants WHERE invitation_status = 'Belum'"""
cursor.execute(sql_select_query)
record = cursor.fetchall()

env = Environment(
    loader=FileSystemLoader('%s/templates/' % os.path.dirname(__file__)))

if len(record) == 0:
    print("All Email Sent!")

else:
    from_email = 'webinar.swe.seaxhmtk@gmail.com'
    server = SMTP_SSL('smtp.googlemail.com', 465)
    server.login(from_email, 'seaxhmtk')
    sent = 0
    for participant in record : 
        to_email = participant[1]
        subject = 'Your E-Ticket for Career Preparation As A Software Engineer Webinar'
        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = "Webinar Career Preparation As a Software Engineer by SEA x HMTK Telkom University"
        message['To'] = to_email
        template = env.get_template('remind.html')
        bodyContent = template.render(data=participant[2]) 
        message.attach(MIMEText(bodyContent, "html"))
        msgBody = message.as_string()

        email_sent = False
        try : 
            server.sendmail(from_email, to_email, msgBody)
            print("Email sent to : ", participant[1])
            email_sent = True
        except : 
            print("Failed to send email to : ", participant[1])
        
        if email_sent : 
            try : 
                sql_update_query = "UPDATE participants SET invitation_status = 'Sudah' WHERE id = %s"
                val = (participant[0],)
                cursor.execute(sql_update_query, val)
                db.mydb.commit()
                print("Database Record Updated")
                sent+=1
            except : 
                print("Database query failed")

    print(f"Successfully sent {sent} emails")
    server.quit()